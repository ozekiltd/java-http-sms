This Java SMS source code can be used to send SMS text messages to mobile phones from a Java SMS application by using HTTP requests and responses.

If you want to send SMS messages from a Java SMS application, HTTP provides the simplest method to send data from your application to another, since Java has native method calls to submit HTTP requests. 

It allows you to transmit SMS messages to you SMS gateway by using HTTP GET or HTTP POST method.

**Prerequisites to implement this solution:**

* [Ozeki NG SMS Gateway](http://ozekisms.com/index.php?owpn=112&info=download-sms-gateway)

* [Java SE Development Kit](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

* [Eclipse IDE for Java developers](http://www.eclipse.org/downloads/)